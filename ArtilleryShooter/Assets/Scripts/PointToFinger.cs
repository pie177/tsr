﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointToFinger : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		if(Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }

        if(Input.touchCount > 0)
        {
            Vector2 touch = Input.GetTouch(0).position;
            Vector3 target = new Vector3(touch.x, touch.y, 0);
            Vector3 scrPoint = Camera.main.ScreenToWorldPoint(target);
            Vector3 dir = scrPoint - this.transform.position;
            float rotation = Mathf.Atan2(dir.y, dir.x);
            rotation = rotation * Mathf.Rad2Deg;
            this.transform.rotation = Quaternion.Euler(0, 0, rotation);
        }

        
    }
}
