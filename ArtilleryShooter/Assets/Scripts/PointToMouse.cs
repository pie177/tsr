﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointToMouse : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        Vector3 scrPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        scrPoint.z = 0;
        Vector3 dir = scrPoint - transform.position;
        float rotation = Mathf.Atan2(dir.y, dir.x);
        rotation = rotation * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0, 0, rotation);
	}
}
