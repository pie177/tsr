﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UICode : MonoBehaviour {

    public GameObject targetCircle;
    public GameObject playerCircle;

    GameObject currTarget;
    GameObject currPlayer;

    bool created;

	// Use this for initialization
	void Start ()
    {
        created = false;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

    public void SpawnCircles()
    {

        if(created)
        {
            Destroy(currTarget);
            Destroy(currPlayer);
        }

        //spawn code!
        float minY = -Camera.main.orthographicSize + 2;
        float maxY = Camera.main.orthographicSize - 2;
        float maxX = Camera.main.orthographicSize * Camera.main.aspect -2.5f;
        float minX = -maxX;

        float rotation = Random.Range(0, 359);

        float posX = Random.Range(minX, maxX);
        float posY = Random.Range(minY, maxY);
        Vector3 pos = new Vector3(posX, posY, 0);

        currTarget = Instantiate(targetCircle, pos, Quaternion.Euler(0, 0, rotation));

        rotation = Random.Range(0, 359);
        posX = Random.Range(minX, maxX);
        posY = Random.Range(minY, maxY);
        pos = new Vector3(posX, posY, 0);
        currPlayer = Instantiate(playerCircle, pos, Quaternion.Euler(0, 0, rotation));

        created = true;

    }
}
