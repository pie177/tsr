﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerControl : MonoBehaviour {

    public BoxCollider2D rotateTrigger;
    //public BoxCollider2D dragTrigger;

    public GameObject bullet;

    float startTime = 0f;

    public enum Stages
    {
        WaitingForRotate,
        Rotating,
        WaitingForDrag,
        Dragging,
        Scoring,
        Done
    }

    public Stages stage = Stages.WaitingForRotate;

	// Use this for initialization
	void Start ()
    {
        startTime = Time.timeSinceLevelLoad;
	}
	
	// Update is called once per frame
	void Update ()
    {
		if(stage == Stages.Rotating && Input.touchCount > 0)
        {
            HandleRotating();
        }
        else if(stage == Stages.Rotating && Input.touchCount == 0)
        {
            stage = Stages.WaitingForDrag;
            Debug.Log("Waiting for drag");
        }
        else if(stage == Stages.Dragging && Input.touchCount > 0)
        {
            HandleShooting();
        }
        else if(stage == Stages.Dragging && Input.touchCount == 0)
        {
            stage = Stages.Scoring;
            ScorePoints();
            print("stage is now scoring");
        }
        else if(stage == Stages.Scoring)
        {
            stage = Stages.Done;
            print("stage is now done");
        }
	}

    private void ScorePoints()
    {
        //GameObject target = GameObject.FindWithTag("Target");
        //Vector3 direction = target.transform.position - transform.position;
        //float distance = direction.magnitude;

        //Vector3 targetRot = target.transform.rotation.eulerAngles;
        //Vector3 thisRot = transform.rotation.eulerAngles;
        //float rotation = Mathf.Abs(targetRot.z - thisRot.z);

        /*UnityEngine.UI.Text tempText;

        tempText = GameObject.FindWithTag("DistanceText").GetComponent<UnityEngine.UI.Text>();
        tempText.text = "Distance: " + distance;

        tempText = GameObject.FindWithTag("RotationText").GetComponent<UnityEngine.UI.Text>();
        tempText.text = "Angle: " + rotation;

        tempText = GameObject.FindWithTag("TimeText").GetComponent<UnityEngine.UI.Text>();
        tempText.text = "Time " + (Time.timeSinceLevelLoad - startTime);*/
    }

    private void HandleDragging()
    {
        Vector2 touchPoint = Input.GetTouch(0).position;
        Vector3 worldPoint = Camera.main.ScreenToWorldPoint(new Vector3(touchPoint.x, touchPoint.y, 0));
        worldPoint.z = 0;
        transform.position = worldPoint;
    }

    private void OnMouseDown()
    {
        if(stage == Stages.WaitingForRotate)
        {
            //rotateTrigger.enabled = false;
            //dragTrigger.enabled = true;
            stage = Stages.Rotating;
            Debug.Log("Stage is rotating");
        }
        else if(stage == Stages.WaitingForDrag)
        {
            //dragTrigger.enabled = false;
            stage = Stages.Dragging;
            Debug.Log("Stage is now dragging");
        }
    }

    void HandleRotating()
    {
        Vector2 touchPoint = Input.GetTouch(0).position;
        Vector3 worldPoint = Camera.main.ScreenToWorldPoint(new Vector3(touchPoint.x, touchPoint.y, 0));
        worldPoint.z = 0;
        Vector3 dir = worldPoint - transform.position;
        float rotation = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0, 0, rotation);
    }

    private void HandleShooting()
    {
        Vector2 touchPoint = Input.GetTouch(0).position;
        Vector3 worldPoint = Camera.main.ScreenToWorldPoint(new Vector3(touchPoint.x, touchPoint.y, 0));
        worldPoint.z = 0;
        GameObject tempobj = Instantiate(bullet, transform.position, Quaternion.identity);
        tempobj.transform.position = Vector3.Lerp(worldPoint, transform.position, 3 * Time.deltaTime);
    }
}
